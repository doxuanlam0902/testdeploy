package com.esdo.bepilot.Repository;

import com.esdo.bepilot.Model.Entity.MissionDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MissionDetailRepository extends JpaRepository<MissionDetail, Long> {


    /*
    * lấy các missionDetail theo id của User
    *
    *
     */
}
