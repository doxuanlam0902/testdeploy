package com.esdo.bepilot.Repository;

import com.esdo.bepilot.Model.Entity.Withdrawn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WithdrawnRepository extends JpaRepository<Withdrawn , Long> {
}
