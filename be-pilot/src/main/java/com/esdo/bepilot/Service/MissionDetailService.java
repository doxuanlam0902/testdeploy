package com.esdo.bepilot.Service;

import com.esdo.bepilot.Model.Entity.MissionDetail;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MissionDetailService {

    MissionDetail create(MissionDetail missionDetail);

    List<MissionDetail> getAllMissionDetail();

    MissionDetail getMissionDetailById(Long id);

    String deleteMissionDetailById(Long id) ;

    MissionDetail updateMissionDetailById(MissionDetail newMissionDetail) ;
}
