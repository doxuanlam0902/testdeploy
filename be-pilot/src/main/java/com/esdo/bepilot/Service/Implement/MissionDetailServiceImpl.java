package com.esdo.bepilot.Service.Implement;

import com.esdo.bepilot.Model.Entity.MissionDetail;
import com.esdo.bepilot.Repository.MissionDetailRepository;
import com.esdo.bepilot.Service.MissionDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class MissionDetailServiceImpl implements MissionDetailService {

    @Autowired
    public MissionDetailRepository missionDetailRepository ;

    public MissionDetail create(MissionDetail missionDetail){
        log.info("Inside create of MissionDetail Service ");
//        missionDetailRepository.save(missionDetail) ;
        return null ;
    }

    public List<MissionDetail> getAllMissionDetail(){
        log.info("Inside getAllMissionDetail of MissionDetail Service ");
//        missionDetailRepository.findAll() ;
        return null ;
    }

    public MissionDetail getMissionDetailById(Long id) {
        log.info("Inside getMissionDetailById of MissionDetail Service ");
//        missionDetailRepository.getById(id) ;
        return null ;
    }

    public String deleteMissionDetailById(Long id) {
        log.info("Inside deleteMissionDetailById of MissionDetail Service ");
//        missionDetailRepository.deleteById(id);
        return "" ;
    }

    public MissionDetail updateMissionDetailById(MissionDetail newMissionDetail) {
        log.info("Inside updateMissionDetailById of MissionDetail Service ");
//        missionDetailRepository.save(newMissionDetail);
        return null ;
    }
}