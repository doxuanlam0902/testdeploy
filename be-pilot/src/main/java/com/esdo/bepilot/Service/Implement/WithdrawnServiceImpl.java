package com.esdo.bepilot.Service.Implement;

import com.esdo.bepilot.Model.Entity.Withdrawn;
import com.esdo.bepilot.Model.Response.WithdrawnResponse;
import com.esdo.bepilot.Repository.WithdrawnRepository;
import com.esdo.bepilot.Service.WithdrawnService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class WithdrawnServiceImpl implements WithdrawnService {

    @Autowired
    WithdrawnRepository withdrawnRepository ;

    public Withdrawn create(Withdrawn withdrawn){
        log.info("Inside create of Withdrawn Service ");
//        withdrawnRepository.save(withdrawn) ;
        return null ;
    }

    public List<WithdrawnResponse> getAllWithdrawn(){
        log.info("Inside getAllWithdrawn of Withdrawn Service ");
//        withdrawnRepository.findAll() ;
        return null ;
    }

    public WithdrawnResponse getWithdrawnByUserId(Long id) {
        log.info("Inside getWithdrawnById of Withdrawn Service ");

        //TODO lấy withdrawn theo id người dùng
//        withdrawnRepository.getById(id) ;
        return null ;
    }

    public String deleteWithdrawnById(Long id) {
        log.info("Inside deleteWithdrawnById of Withdrawn Service ");
//        withdrawnRepository.deleteById(id);
        return "" ;
    }

    public WithdrawnResponse updateWithdrawnById(Withdrawn newWithdrawn) {
        log.info("Inside updateWithdrawnById of Withdrawn Service ");
//        withdrawnRepository.save(newWithdrawn);
        return null ;
    }
}